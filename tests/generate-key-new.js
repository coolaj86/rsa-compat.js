'use strict';

var RSA = require('../').RSA;

RSA.generateKeypair(null, function (err, keys) {
  if (!keys || !keys.privateKeyJwk) {
    throw new Error("Expected privateKeyJwk, but it is missing");
  }

  var options = {
    public: true        // export public keys
  , pem: true           // export pems
  , jwk: false          // export jwks
  , internal: true      // preserve internal intermediate formats (_ursa, _forge)
  //, thumbprint: true    // JWK sha256 thumbprint
  , bitlen: 2048
  , exp: 65537
  };
  RSA.generateKeypair(options, function (err, keys) {
    if (
        (keys.publicKeyJwk && !keys.thumbprint)
    || !keys.privateKeyPem
    || !keys.publicKeyPem
    //|| !keys.thumbprint
    ) {
      console.error(Object.keys(keys));
      throw new Error("Missing expected keys");
    }

    console.log('All is well!');
  });

});
