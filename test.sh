#!/bin/bash
set -e

node tests/generate-csr.js
node tests/generate-key.js
node tests/generate-key-new.js
node tests/generate-sig.js
node tests/reciprocate.js
